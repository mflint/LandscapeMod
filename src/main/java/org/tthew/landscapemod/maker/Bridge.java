package org.tthew.landscapemod.maker;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.tthew.landscapemod.util.ModMath;
import org.tthew.landscapemod.util.ModMath.ModVector;

import java.util.*;

public final class Bridge {
    private final IBlockState blockState;
    private final BlockPos start;
    private final BlockPos end;
    private final int width;
    private final int depth;

    public Bridge(final IBlockState blockState, final BlockPos start, final BlockPos end, final int width, final int depth) {
        this.blockState = blockState;
        this.start = start;
        this.end = end;
        this.width = width;
        this.depth = depth;
    }

    public void build(final World world) {
        final ModVector vector = ModMath.vectorBetween(start, end);
        final double xEnd = Math.floor(this.width / 2.0);
        final double xStart = xEnd + 1 - this.width;
        final Stack<BlockPos> blocksToPlace = new Stack<>();
        for(double z = 0; z <= vector.distance; z++) {
            for (double y = 0; y > -this.depth; y--) {
                for (double x = xStart; x <= xEnd; x++) {
                    final BlockPos blockPos = ModMath.blockPos(start, vector, x, y, z);
                    blocksToPlace.push(blockPos);
                }
            }
        }

        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                int taken = 0;
                while (!blocksToPlace.empty() && taken < 3) {
                    final BlockPos blockPos = blocksToPlace.pop();
                    taken++;
                    world.setBlockState(blockPos, blockState);
                }

                if (blocksToPlace.empty()) {
                    cancel();
                }
            }
        };
        new Timer(false).scheduleAtFixedRate(task, 0, 200);
    }
}

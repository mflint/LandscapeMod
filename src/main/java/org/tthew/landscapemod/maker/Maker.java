package org.tthew.landscapemod.maker;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface Maker {
    boolean accept(BlockPos pos, World world);
    void remove(BlockPos pos);
}

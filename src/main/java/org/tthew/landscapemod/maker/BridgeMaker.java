package org.tthew.landscapemod.maker;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public final class BridgeMaker implements Maker {
    private List<BlockPos> positions = new ArrayList<BlockPos>();
    private IBlockState blockState;

    @Override
    public boolean accept(final BlockPos pos, final World world) {
        if (world.isAirBlock(pos)) {
            return false;
        }

        if (positions.size() == 0) {
            positions.add(pos);
            blockState = world.getBlockState(pos);
            return true;
        }

        if (positions.size() == 1) {
            positions.add(pos);
            buildBridge(world);
            return true;
        }

        return false;
    }

    @Override
    public void remove(final BlockPos pos) {
        positions.remove(pos);
    }

    private void buildBridge(final World world) {
        new Timer(false).schedule(new TimerTask() {
            @Override
            public void run() {
                final Bridge bridge = new Bridge(blockState, positions.get(0), positions.get(1), 7, 1);
                doRemoveBlocks(world);
                doBuildBridge(bridge, world);
            }
        }, 1000);
    }

    private void doRemoveBlocks(final World world) {
        for (final BlockPos position : positions) {
            world.setBlockToAir(position.up());
        }
        positions.clear();
    }

    private void doBuildBridge(final Bridge bridge, final World world) {
        bridge.build(world);
    }
}

package org.tthew.landscapemod.util;

import net.minecraft.util.math.BlockPos;

public final class ModMath {
    public static final class ModVector {
        public final int distance;
        public final double rotation;
        public final double elevation;

        public ModVector(final int distance, final double rotation, final double elevation) {
            this.distance = distance;
            this.rotation = rotation;
            this.elevation = elevation;
        }

        @Override
        public String toString() {
            return "[ModVector distance:" + distance + ",rotation:" + rotation + ",elevation:" + elevation + "]";
        }
    }

    public static final ModVector vectorBetween(final BlockPos first, final BlockPos second) {
        final double xDistanceOnGround = second.getX() - first.getX();
        final double zDistanceOnGround = second.getZ() - first.getZ();
        final double distanceOnGround = Math.sqrt((xDistanceOnGround * xDistanceOnGround) + (zDistanceOnGround * zDistanceOnGround));
        final int yDistance = second.getY() - first.getY();
        final int distance = (int)Math.ceil(Math.sqrt((distanceOnGround * distanceOnGround) + (yDistance * yDistance)));
        final double rotation = Math.atan2(xDistanceOnGround, zDistanceOnGround);
        final double elevation = Math.atan(yDistance / distanceOnGround);
        return new ModVector(distance, rotation, elevation);
    }

    public static BlockPos blockPos(final BlockPos start, final ModVector vector, final double x, final double y, final double z) {
        final double elevatedX = x;
        final double elevatedY = (z * Math.sin(vector.elevation)) + (y * Math.cos(vector.elevation));
        final double elevatedZ = (z * Math.cos(vector.elevation)) - (y * Math.sin(vector.elevation));

        final double rotatedX = (elevatedX * Math.cos(vector.rotation)) + (elevatedZ * Math.sin(vector.rotation));
        final double rotatedY = elevatedY;
        final double rotatedZ = (elevatedX * Math.sin(vector.rotation)) + (elevatedZ * Math.cos(vector.rotation));

        final BlockPos result = new BlockPos(rotatedX + start.getX(), rotatedY + start.getY(), rotatedZ + start.getZ());
        return result;
    }
}

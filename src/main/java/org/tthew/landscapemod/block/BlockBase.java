package org.tthew.landscapemod.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import org.tthew.landscapemod.LandscapeMod;

public class BlockBase extends Block {
    private final String name;

    public BlockBase(final Material material, final String name) {
        super(material);

        this.name = name;

        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerBlockModel(final ItemBlock itemBlock) {
        LandscapeMod.proxy.registerItemRenderer(itemBlock, 0, name);
    }

    @Override
    public BlockBase setCreativeTab(final CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }

//    @Override
//    public boolean onBlockActivated(final World worldIn, final BlockPos pos, final IBlockState state, final EntityPlayer playerIn, final EnumHand hand, final EnumFacing facing, final float hitX, final float hitY, final float hitZ) {
//        System.out.println("LANDSCAPE: onBlockActivated " + state.getBlock().getUnlocalizedName());
//        return true;
//    }
}

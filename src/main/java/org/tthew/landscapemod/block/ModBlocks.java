package org.tthew.landscapemod.block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.tthew.landscapemod.tabs.LandscapeTab;

public class ModBlocks {
    private static BlockBase bridgeBlock;

    public static void init() {
        bridgeBlock = register(new BridgeBlock().setCreativeTab(LandscapeTab.LANDSCAPE_TAB));
    }

    private static <T extends BlockBase> T register(final T block, final ItemBlock itemBlock) {
        GameRegistry.register(block);
        GameRegistry.register(itemBlock);

        block.registerBlockModel(itemBlock);

        return block;
    }

    private static <T extends BlockBase> T register(final T block) {
        ItemBlock itemBlock = new ItemBlock(block);
        itemBlock.setRegistryName(block.getRegistryName());
        return register(block, itemBlock);
    }
}

package org.tthew.landscapemod.block;

import net.minecraft.block.material.Material;

public class BridgeBlock extends BlockBase {
    public BridgeBlock() {
        super(Material.CARPET, "bridgeBlock");
    }
}

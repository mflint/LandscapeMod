package org.tthew.landscapemod.event;

import net.minecraftforge.common.MinecraftForge;

public class ModEventHandler {
    public static void init() {
        System.out.println("LANDSCAPE: registering BlockEventHandler");
        MinecraftForge.EVENT_BUS.register(new BlockEventHandler());
    }
}

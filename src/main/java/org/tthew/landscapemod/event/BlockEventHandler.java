package org.tthew.landscapemod.event;

import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.tthew.landscapemod.block.BlockBase;
import org.tthew.landscapemod.block.BridgeBlock;
import org.tthew.landscapemod.maker.BridgeMaker;
import org.tthew.landscapemod.maker.Maker;

import java.util.HashMap;
import java.util.Map;

public class BlockEventHandler {
    private Map<Class<? extends BlockBase>, Maker> constructions = new HashMap<>();

    public BlockEventHandler() {
        constructions.put(BridgeBlock.class, new BridgeMaker());
    }

    @SubscribeEvent
    public void blockPlacing(final PlaceEvent placeEvent) {
        System.out.println("LANDSCAPE: placed " + placeEvent.getPlacedBlock().getBlock().getUnlocalizedName() + " at " + placeEvent.getPos().toString());

        final Maker construction = constructions.get(placeEvent.getPlacedBlock().getBlock().getClass());
        if (construction != null) {
            if (!construction.accept(placeEvent.getPos().down(), placeEvent.getWorld())) {
                placeEvent.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void blockBreaking(final BreakEvent breakEvent) {
        final Maker construction = constructions.get(breakEvent.getState().getBlock().getClass());
        if (construction != null) {
            construction.remove(breakEvent.getPos());
        }
    }
}

package org.tthew.landscapemod;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.tthew.landscapemod.proxy.CommonProxy;

@Mod(modid = LandscapeMod.modId, name = LandscapeMod.name, version = LandscapeMod.version)
public class LandscapeMod {
    @SidedProxy(serverSide = "org.tthew.landscapemod.proxy.CommonProxy", clientSide = "org.tthew.landscapemod.proxy.ClientProxy")
    public static CommonProxy proxy;

    public static final String modId = "landscape";
    public static final String name = "Landscape Mod";
    public static final String version = "1.0.0";

    @Mod.Instance(modId)
    public static LandscapeMod instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        System.out.println(name + " is loading!");
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
}

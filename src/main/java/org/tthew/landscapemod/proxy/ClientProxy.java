package org.tthew.landscapemod.proxy;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.tthew.landscapemod.LandscapeMod;
import org.tthew.landscapemod.block.ModBlocks;
import org.tthew.landscapemod.event.ModEventHandler;

/**
 * Created by matthew on 14/05/2017.
 */
public final class ClientProxy extends CommonProxy {
    @Override
    public void preInit(final FMLPreInitializationEvent event) {
        super.preInit(event);
        ModBlocks.init();
    }

    @Override
    public void init(final FMLInitializationEvent event) {
        super.init(event);
        ModEventHandler.init();
    }

    @Override
    public void postInit(final FMLPostInitializationEvent event) {
        super.postInit(event);
    }

    @Override
    public void registerItemRenderer(final Item item, final int meta, final String id) {
        super.registerItemRenderer(item, meta, id);
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(LandscapeMod.modId + ":" + id, "inventory"));
    }
}

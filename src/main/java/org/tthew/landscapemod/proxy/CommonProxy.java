package org.tthew.landscapemod.proxy;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by matthew on 14/05/2017.
 */
public abstract class CommonProxy {
    public void preInit(final FMLPreInitializationEvent event) {

    }

    public void init(final FMLInitializationEvent event) {
    }

    public void postInit(final FMLPostInitializationEvent event) {
    }

    public void registerItemRenderer(final Item item, final int meta, final String id) {
    }
}

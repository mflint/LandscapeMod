package org.tthew.landscapemod.tabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LandscapeTab extends CreativeTabs {
    public static final LandscapeTab LANDSCAPE_TAB = new LandscapeTab("Landscape");

    public LandscapeTab(final String label) {
        super(label);
        // this.setBackgroundImageName("background.png");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(Items.LAVA_BUCKET);
    }
}
